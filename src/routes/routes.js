import DashboardLayout from '@/pages/Layout/DashboardLayout.vue'

import Dashboard from '@/pages/Dashboard.vue'
import UserProfile from '@/pages/UserProfile.vue'
import TableList from '@/pages/TableList.vue'
import Typography from '@/pages/Typography.vue'
import Icons from '@/pages/Icons.vue'
import Maps from '@/pages/Maps.vue'
import Notifications from '@/pages/Notifications.vue'
import UpgradeToPRO from '@/pages/UpgradeToPRO.vue'
import GrantedForm from '@/pages/GrantedForm'

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/user',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: 'user',
        name: 'User Profile',
        component: UserProfile
      },
      {
        path: 'table',
        name: 'Table List',
        component: TableList
      },
      {
        path: 'login',
        name: 'Typography',
        component: Typography
      },
      {
        path: 'i-p-forms/:id/:c_id/:d_id/:email',
        name: 'Form',
        component: Icons
      },
      {
        path: 'granted-i-p-forms/:f_id/:c_id/:d_id/:name',
        name: 'GrantedForm',
        component: GrantedForm
      },
      {
        path: '',
        name: 'Maps',
        component: Maps

      },
      {
        path: 'notifications',
        name: 'Notifications',
        component: Notifications
      },
      {
        path: 'upgrade',
        name: 'Upgrade to PRO',
        component: UpgradeToPRO
      }
    ]
  }
]

export default routes
